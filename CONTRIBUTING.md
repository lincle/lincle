This project follows the [commitizen](https://github.com/commitizen/cz-cli) commit conventions. The first line of every commit message must adhere to the following format:

| **`{type}({scope?}): {title}`** | |
|-|-|
| **type** | Select a type of change: <br />- feat <br />- fix <br />- docs <br />- style <br />- refactor <br />- perf <br />- test <br />- chore <br />- ci <br />- build <br />- revert |
| **scope** (optional) | Denote the scope of this change (ui, jest, node, etc.) |
| **title** | A short, imperative tense title of the change |
| ***length*** | Total length must be less than 100 characters |
| ***examples*** | - `test(jest): Test orientation switch` <br /> - `build: Add CI stage` |
