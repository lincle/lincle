<div align="center">
	<h1>Monorepo for <code>lincle</code>: <br />A <i>reactive</i> React node and edge generator</h1>
</div>

<div align="center">
	<a href="https://commitizen.github.io/cz-cli/"><img alt="Commitizen Friendly" src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000" /></a>
	<a href="https://gitlab.com/hyper-expanse/semantic-release-gitlab#readme"><img alt="Semantic Release" src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000" /></a>
</div>

<div align="center">
	<a href="https://gitlab.com/digested/lincle/commits/master"><img alt="Build Status" src="https://gitlab.com/digested/lincle/badges/master/build.svg" /></a>
	<a href="https://lincle.gitlab.io/lincle/stats/lincle/coverage/lcov-report/"><img alt="Coverage Report" src="https://gitlab.com/digested/lincle/badges/master/coverage.svg" /></a>
	<a href="https://lincle.gitlab.io/lincle/stats/lincle/webpack/"><img alt="Build Analysis" src="https://img.shields.io/badge/webpack-stats-blue.svg" /></a>
	<a href="https://www.npmjs.org/package/@lincle/base"><img alt="NPM Version" src="http://img.shields.io/npm/v/lincle.svg?maxAge=86400" /></a>
	<a href="https://www.gnu.org/licenses/gpl-3.0.en.html"><img alt="License" src="https://img.shields.io/npm/l/lincle.svg?maxAge=2592000" /></a>
	<a href="https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Flincle%2Flincle?ref=badge_shield"><img alt="FOSSA Status" src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Flincle%2Flincle.svg?type=shield&maxAge=3600" /></a>
	<a href="https://github.com/gajus/canonical"><img alt="Canonical Code Style" src="https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000" /></a>
</div>

**lincle** generates a reactive, interactive, touch supported node and edge graph.

### [Demo](https://lincle.gitlab.io/lincle/)

This is a mono repo. See [here for package source](https://gitlab.com/digested/lincle/tree/master/packages/) and [here for demo source](https://gitlab.com/digested/lincle/tree/master/demo/).


## Build

```bash
yarn bootstrap
yarn build
```

## Demo

```bash
yarn start
```

## Development

The following will run, in parallel, `webpack` in `watch` mode for the packages, and start an express development server for the demo:

```bash
yarn start:dev
```
