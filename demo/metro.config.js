/*

Copyright (C) 2016,2017 wallzero 2016,2017

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>.

*/

/*
  eslint-disable
  unicorn/prefer-module
*/

/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 */

const {
  getDefaultConfig,
  mergeConfig
} = require('@react-native/metro-config');
const path = require('node:path');

/**
 * Metro configuration
 * https://reactnative.dev/docs/metro
 * @type {import('metro-config').MetroConfig}
 */
const config = {
  resolver: {
    disableHierarchicalLookup: true,
    nodeModulesPaths: [
      path.join(
        __dirname,
        'node_modules'
      )
    ]
  },
  watchFolders: [
    path.resolve(
      __dirname,
      '..',
      'packages'
    )
  ]
};

module.exports = mergeConfig(
  getDefaultConfig(__dirname),
  config
);
