/*
  eslint-disable
  unicorn/prevent-abbreviations
*/
import React from 'react';

// eslint-disable-next-line n/no-process-env
if (process.env.NODE_ENV === 'development') {
  /*
  // eslint-disable-next-line promise/prefer-await-to-then
  import('react-dom').then(
     (ReactDOM): void => {
        // @ts-ignore
        return import('react-axe').then( // eslint-disable-line promise/prefer-await-to-then
           // eslint-disable-next-line no-undef,@typescript-eslint/no-explicit-any
           (axe: any): void => {
              return axe(React, ReactDOM, 5000, {
                 rules: [{
                    enabled: false,
                    id: 'region'
                 }]
              });
           }
        ).catch((): void => {});
     }
  ).catch((): void => {});
  /**/

  // eslint-disable-next-line promise/prefer-await-to-then
  import('@welldone-software/why-did-you-render').then(
    (
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      whyDidYouRender: any
    ): void => {
      return whyDidYouRender(
        React,
        {
          collapseGroups: true,
          groupByComponent: true,
          include: [
            /^Dynamic$/u
          ],
          traceHooks: true,
          trackAllPureComponents: true
        }
      );
    }
  // eslint-disable-next-line promise/prefer-await-to-then
  ).catch(
    (): void => {}
  );

  /**/
}

export {
  default
} from './App';
