/*
  eslint-disable
  import/no-unassigned-import
*/

import './dev';
import './vendor';
import './index.g.css';
import App from './App';
import React from 'react';
import {
  createRoot
} from 'react-dom/client';

const rootElement = document.querySelector('#app') as HTMLElement;

const root = createRoot(
  rootElement
);

root.render(<App />);

export {
  default
} from './App';
