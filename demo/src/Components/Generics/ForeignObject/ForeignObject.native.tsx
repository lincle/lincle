import React, {
  type FunctionComponent,
  type SVGProps,
  useCallback
} from 'react';
import {
  ForeignObject as NativeForeignObject,
  type ForeignObjectProps as NativeForeignObjectProps
} from 'react-native-svg';

const ForeignObject: FunctionComponent<ForeignObjectProps> = ({
  onClick,
  ...props
}) => {
  const handleOnPress = useCallback(
    () => {
      if (onClick) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        onClick();
      }
    },
    [
      onClick
    ]
  );

  return (
    <NativeForeignObject
      onPress={handleOnPress}
      {...props as NativeForeignObjectProps}
    />
  );
};

export {
  ForeignObject
};

export type ForeignObjectProps = NativeForeignObjectProps & Partial<WebForeignObjectProps>;

type WebForeignObjectProps = SVGProps<SVGForeignObjectElement>;
