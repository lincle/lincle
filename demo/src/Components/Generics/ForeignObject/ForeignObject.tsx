import React, {
  type FunctionComponent,
  type SVGProps,
  useMemo
} from 'react';

const ForeignObject: FunctionComponent<ForeignObjectProps> = (props) => {
  const style = useMemo(
    () => {
      return {
        overflow: 'visible',
        zIndex: 1_000_000,
        ...props.style
      };
    },
    [
      props.style
    ]
  );

  return (
    <foreignObject
      style={style}
      {...props}
    />
  );
};

export {
  ForeignObject
};

export type ForeignObjectProps = SVGProps<SVGForeignObjectElement>;
