import React, {
  type CSSProperties,
  type FunctionComponent,
  type SVGProps
} from 'react';

const style: CSSProperties = {
  height: 1,
  overflow: 'visible',
  pointerEvents: 'none',
  width: 1
};

// eslint-disable-next-line id-length
const G: FunctionComponent<GProps> = (props) => {
  return (
    <g
      {...props}
      style={style}
    />
  );
};

export {
  G
};

export type GProps = SVGProps<SVGGElement>;
