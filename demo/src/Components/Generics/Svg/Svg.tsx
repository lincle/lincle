import React, {
  type CSSProperties,
  type FunctionComponent,
  type SVGProps
} from 'react';

const style: CSSProperties = {
  overflow: 'visible',
  pointerEvents: 'none'
};

const Svg: FunctionComponent<SvgProps> = (props) => {
  return (
    <svg
      {...props}
      style={style}
    />
  );
};

export {
  Svg
};

export type SvgProps = SVGProps<SVGSVGElement>;
