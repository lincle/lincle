import React, {
  type FunctionComponent,
  type HTMLAttributes,
  useCallback
} from 'react';
import {
  Text as NativeText,
  type TextProps as NativeTextProps
} from 'react-native';

const Text: FunctionComponent<TextProps> = ({
  children,
  onClick,
  ...props
}) => {
  const handleOnPress = useCallback(
    () => {
      if (onClick) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        onClick();
      }
    },
    [
      onClick
    ]
  );

  return (
    <NativeText
      onPress={handleOnPress}
      {...props as NativeTextProps}
    >
      {children}
    </NativeText>
  );
};

export {
  Text
};

export type TextProps = NativeTextProps & Partial<WebTextProps>;

type WebTextProps = HTMLAttributes<HTMLSpanElement>;
