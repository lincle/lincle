/*
  eslint-disable
  canonical/filename-match-exported
*/

import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  }
});

const Preliminary: FunctionComponent<PreliminaryProps> = ({
  children
}) => {
  return (
    <SafeAreaView
      style={styles.container}
    >
      <ScrollView
        contentContainerStyle={styles.container}
      >
        {children}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Preliminary;

export type PreliminaryProps = PropsWithChildren<object>;
