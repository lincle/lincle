import {
  type ConnectContextProps
} from '../types';
import {
  createContext
} from 'react';

const ConnectContext = createContext<ConnectContextProps>({});

ConnectContext.displayName = 'LincleInteractiveScaleContext';

export {
  ConnectContext
};
