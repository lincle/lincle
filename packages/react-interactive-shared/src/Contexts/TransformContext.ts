import defaultSettings from '../defaultSettings';
import {
  createContext
} from 'react';

const transform = defaultSettings.transform;

const TransformContext = createContext<TransformContextProps>(transform);

TransformContext.displayName = 'LincleInteractiveTransformContext';

export {
  TransformContext
};

export type TransformContextProps = {
  maxScale: number;
  minScale: number;
  onScale?: (scale: number) => void;
  zoom: boolean;
};
