import defaultSettings from '../defaultSettings';
import {
  type Translate
} from '../types';
import {
  createContext
} from 'react';

const translate = defaultSettings.translate;

const TranslateContext = createContext<TranslateContextProps>(translate);

TranslateContext.displayName = 'LincleInteractiveTranslateContext';

export {
  TranslateContext
};

export type TranslateContextProps = {
  onTranslate?: (translate: Translate) => void;
  pan: boolean;
  translate: Translate;
};
