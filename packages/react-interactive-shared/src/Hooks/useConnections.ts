import {
  ConnectContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useConnections = () => {
  const {
    connections = {}
  } = useContext(ConnectContext);

  return connections;
};

export {
  useConnections
};
