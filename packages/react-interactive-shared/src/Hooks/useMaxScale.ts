import {
  TransformContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useMaxScale = () => {
  const {
    maxScale
  } = useContext(TransformContext);

  return maxScale;
};

export {
  useMaxScale
};
