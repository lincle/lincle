import {
  TransformContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useMinScale = () => {
  const {
    minScale
  } = useContext(TransformContext);

  return minScale;
};

export {
  useMinScale
};
