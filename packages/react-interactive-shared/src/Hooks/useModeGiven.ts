import {
  ModeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useModeGiven = () => {
  const {
    givenMode
  } = useContext(ModeContext);

  return givenMode;
};

export {
  useModeGiven
};
