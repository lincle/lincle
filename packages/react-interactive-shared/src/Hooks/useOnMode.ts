import {
  ModeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnMode = () => {
  const {
    onMode
  } = useContext(ModeContext);

  return onMode;
};

export {
  useOnMode
};
