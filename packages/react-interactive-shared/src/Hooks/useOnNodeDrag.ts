import {
  NodeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnNodeDrag = () => {
  const {
    onNodeDrag
  } = useContext(NodeContext);

  return onNodeDrag;
};

export {
  useOnNodeDrag
};
