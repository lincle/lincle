import {
  NodeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useOnNodeStop = () => {
  const {
    onNodeStop
  } = useContext(NodeContext);

  return onNodeStop;
};

export {
  useOnNodeStop
};
