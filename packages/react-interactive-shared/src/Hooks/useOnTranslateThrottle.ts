import {
  TranslateContext
} from '../Contexts';
import {
  useEdgeFrequency
} from '@lincle/react-shared';
import throttle from 'lodash.throttle';
import {
  useCallback,
  useContext,
  useMemo
} from 'react';

const scaleCoordinates = (
  width: number,
  height: number,
  x: number,
  y: number,
  scale: number
) => {
  if (scale === 1) {
    return {
      x,
      y
    };
  }

  const centerX = width / 2;
  const centerY = height / 2;
  const relX = x - centerX;
  const relY = y - centerY;
  const scaledX = relX * scale;
  const scaledY = relY * scale;
  return {
    x: Math.round(scaledX + centerX),
    y: Math.round(scaledY + centerY)
  };
};

const useOnTranslateThrottle = () => {
  const edgeFrequency = useEdgeFrequency();

  const {
    onTranslate
  } = useContext(TranslateContext);

  const handleTranslate = useCallback(
    (
      width: number,
      height: number,
      x: number,
      y: number,
      scale: number
    ) => {
      if (onTranslate) {
        const scaledCoordinates = scaleCoordinates(
          width,
          height,
          x,
          y,
          scale
        );

        onTranslate(scaledCoordinates);
      }
    },
    [
      onTranslate
    ]
  );

  const throttledTranslate = useMemo(
    () => {
      return throttle(
        handleTranslate,
        edgeFrequency,
        {
          trailing: true
        }
      );
    },
    [
      edgeFrequency,
      handleTranslate
    ]
  );

  return throttledTranslate;
};

export {
  useOnTranslateThrottle
};
