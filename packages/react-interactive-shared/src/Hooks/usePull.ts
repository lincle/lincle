import {
  ModeContext
} from '../Contexts';
import {
  useContext
} from 'react';

const usePull = () => {
  const {
    pull
  } = useContext(ModeContext);

  return pull;
};

export {
  usePull
};
