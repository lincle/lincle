import {
  ScaleContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useScale = () => {
  const {
    scale
  } = useContext(ScaleContext);

  return scale;
};

export {
  useScale
};
