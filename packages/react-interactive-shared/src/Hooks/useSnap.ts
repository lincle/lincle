import {
  SnapContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useSnap = () => {
  const snap = useContext(SnapContext);

  return snap;
};

export {
  useSnap
};
