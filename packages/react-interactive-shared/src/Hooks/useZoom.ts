import {
  TransformContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useZoom = () => {
  const {
    zoom
  } = useContext(TransformContext);

  return zoom;
};

export {
  useZoom
};
