import {
  NodeContext,
  type NodeContextProps
} from '../Contexts';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo,
  useState
} from 'react';

const NodeProvider: FunctionComponent<NodeProviderProps> = ({
  children,
  onNodeDrag,
  onNodeEdgeDrop,
  onNodeSelect,
  onNodeStart,
  onNodeStop
}) => {
  const [
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    nodeControls,
    setNodeControls
  ] = useState({
    onNodeDrag,
    onNodeEdgeDrop,
    onNodeSelect,
    onNodeStart,
    onNodeStop
  });

  const NodeControls = useMemo(
    () => {
      return {
        onNodeDrag,
        onNodeEdgeDrop,
        onNodeSelect,
        onNodeStart,
        onNodeStop,
        setNodeControls
      };
    },
    [
      onNodeDrag,
      onNodeEdgeDrop,
      onNodeSelect,
      onNodeStart,
      onNodeStop,
      setNodeControls
    ]
  );

  return (
    <NodeContext.Provider
      value={NodeControls}
    >
      {children}
    </NodeContext.Provider>
  );
};

NodeProvider.displayName = 'LincleNodeProvider';

export {
  NodeProvider
};

export type NodeProviderProps = PropsWithChildren<NodeContextProps>;
