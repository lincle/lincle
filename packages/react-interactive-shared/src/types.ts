import {
  type ProvidersProps
} from './Providers';
import {
  type GraphProps as BaseGraphProps,
  type GridProps as BaseGridProps,
  type NodeProps as BaseNodeProps,
  type EdgeNodeProps,
  type EdgeProps,
  type GridType,
  type Line
} from '@lincle/react-shared';
import {
  type Dispatch,
  type PropsWithChildren,
  type ReactElement,
  type SetStateAction
} from 'react';

/**
 * Control Props
 */

// export type NodeControlProps = {};

// export type InteractionControlProps = {};

// export type GraphNodeControlProps = {};

/**
 * Context Props
 */

export type ConnectContextProps = {
  connections?: Connections;
  setConnection?: (
    sourceId: string,
    connection?: Connection
  ) => void;
  setConnections?: Dispatch<SetStateAction<Connections>>;
};

export type Connection = {
  line: Line;
  source: EdgeNodeProps;
  target: EdgeNodeProps;
};

export type Connections = {
  [key: string]: Connection;
};

export type ConnectionsProps = PropsWithChildren<{
  readonly scale: number;
  readonly translate: {
    x: number;
    y: number;
  };
}>;

export type DraggableData = {
  deltaX: number;
  deltaY: number;
  lastX: number;
  lastY: number;
  node?: Element;
  x: number;
  y: number;
};

export type EdgeControlProps = {
  onEdgeDrop?: (
    sourceId: number | string,
    targetId?: number | string
  ) => void;
};

export type GraphProps = BaseGraphProps & ProvidersProps;

export type GridProps = Pick<
  BaseGridProps,
  'children'
>;

/**
 * Component Props
 */

export type Mode = {
  mode?: ModeType;
  move: boolean;
  pull: boolean;
};

export type ModeType = 'move' | 'pull' | 'select';

export type MoveNodeProps = {
  readonly disabled: boolean;
  readonly mode?: ModeType;
  readonly onSelect?: (
    nodeId: number | string
  ) => void;
  readonly snap?: false | GridType;
};

export type NodeProps = Omit<BaseNodeProps, 'ref'> &
Partial<Mode> &
Pick<EdgeProps, 'line'> &
{
  move?: boolean;
  pull?: boolean;
};

export type Position = {
  x?: number;
  y?: number;
};

/**
 * GraphContextsProps
 * NodeProps
 * PullProps
 * InteractionProps
 * GraphProps
 */

export type PullNodeProps = {
  readonly disabled?: boolean;
  readonly sourceId: number | string;
};

export type PullProps = {
  diagramId: number | string;
  path?: (
    id: number | string,
    source: EdgeNodeProps,
    target: EdgeNodeProps
  ) => ReactElement;
};

export type Translate = {
  x: number;
  y: number;
};

export {
  type EdgeNodeProps,
  type EdgeProps,
  type EdgesProps,
  type GridType,
  type Line,
  type NodesProps,
  type PathProps,
  type Shape,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape
} from '@lincle/react-shared';
