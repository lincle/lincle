import {
  type EdgeProps,
  useEdge,
  useRemoveEdge,
  useUpdateEdge
} from '../../shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';
import {
  View,
  type ViewStyle
} from 'react-native';

const Edge: FunctionComponent<EdgeProps> = ({
  children,
  dash,
  id: edgeId,
  line,
  markerEnd,
  markerStart,
  // path,
  sourceId,
  targetId
}) => {
  const updateEdge = useUpdateEdge();
  const removeEdge = useRemoveEdge();
  const edge = useEdge(edgeId);

  useEffect(
    () => {
      return (): void => {
        if (removeEdge) {
          removeEdge(
            edgeId
          );
        }
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useEffect(
    () => {
      if (updateEdge) {
        updateEdge(
          edgeId,
          {
            dash,
            line,
            markerEnd,
            markerStart,
            sourceId,
            targetId
          }
        );
      }

      return (): void => {
        if (removeEdge) {
          removeEdge(
            edgeId
          );
        }
      };
    },
    [
      dash,
      edgeId,
      line,
      markerEnd,
      markerStart,
      removeEdge,
      sourceId,
      targetId,
      updateEdge
    ]
  );

  const style = useMemo<ViewStyle>(
    () => {
      return {
        left: edge?.center?.x,
        position: 'absolute',
        top: edge?.center?.y
      };
    },
    [
      edge?.center?.x,
      edge?.center?.y
    ]
  );

  return (
    <View
      style={style}
    >
      {children}
    </View>
  );
};

Edge.displayName = 'LincleEdge';

export {
  Edge
};
