/*
  eslint-disable
  canonical/filename-match-exported
*/
import {
  type EdgeNodeProps,
  type EdgeProps,
  useDefaultLine,
  useEdgeSubscriber
} from '../../../../shared';
import Path from './Path';
import {
  type FunctionComponent,
  useEffect,
  useMemo,
  useState
} from 'react';

const RegisterEdge: FunctionComponent<EdgeProps> = ({
  id: edgeId,
  line: givenLine,
  markerEnd,
  markerStart,
  path,
  sourceId,
  targetId
}) => {
  const defaultLine = useDefaultLine();
  const edgeSubscriber = useEdgeSubscriber();

  const line = givenLine ?? defaultLine;

  const [
    source,
    setSource
  ] = useState<EdgeNodeProps>();

  const [
    target,
    setTarget
  ] = useState<EdgeNodeProps>();

  useEffect(
    (): () => void => {
      setSource(
        edgeSubscriber?.register(
          edgeId,
          sourceId,
          setSource
        )
      );

      setTarget(
        edgeSubscriber?.register(
          edgeId,
          targetId,
          setTarget
        )
      );

      return (): void => {
        edgeSubscriber?.unregister(
          edgeId,
          sourceId
        );

        edgeSubscriber?.unregister(
          edgeId,
          targetId
        );
      };
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );

  const pathSource = useMemo(
    () => {
      return source ?
        {
          ...source,
          x: source.x,
          y: source.y
        } :
        undefined;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      source?.x,
      source?.y
    ]
  );

  const pathTarget = useMemo(
    () => {
      return target ?
        {
          ...target,
          x: target.x,
          y: target.y
        } :
        undefined;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      target?.x,
      target?.y
    ]
  );

  const customPath = useMemo(
    () => {
      return path ?
        path(
          pathSource,
          pathTarget
        ) :
        undefined;
    },

    [
      path,
      pathSource,
      pathTarget
    ]
  );

  if (
    source &&
    target
  ) {
    return customPath ??
    <Path
      center
      edgeId={edgeId}
      line={line}
      markerEnd={markerEnd}
      markerStart={markerStart}
      source={pathSource}
      target={pathTarget}
    />;
  } else {
    return null;
  }
};

RegisterEdge.displayName = 'LincleRegisterEdge';

export default RegisterEdge;

export {
  default as Path
} from './Path';
