import {
  useEdges
} from '../../../shared';
import RegisterEdge from './Path';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const Paths: FunctionComponent<PathsProps> = () => {
  const edges = useEdges();

  const paths = useMemo(
    () => {
      const edgesMap = Object.keys(edges).map(
        (edgeId) => {
          const {
            line,
            sourceId,
            targetId
          } = edges[edgeId];

          if (
            sourceId &&
            targetId
          ) {
            return (
              <RegisterEdge
                id={edgeId}
                key={edgeId}
                line={line}
                sourceId={sourceId}
                targetId={targetId}
              />
            );
          } else {
            return null;
          }
        }
      );

      return edgesMap;
    },
    [
      edges
    ]
  );

  return paths;
};

Paths.displayName = 'LinclePaths';

export type PathsProps = object;

export default Paths;

export {
  Path
} from './Path';
