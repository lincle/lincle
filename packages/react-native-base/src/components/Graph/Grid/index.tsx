import {
  type GridProps,
  useDiagramId,
  useGrid
} from '../../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  StyleSheet
// eslint-disable-next-line import/no-deprecated
} from 'react-native';
import Svg, {
  Circle as NativeCircle,
  Pattern,
  Rect
} from 'react-native-svg';

const styles = StyleSheet.create({
  grid: {
    height: '100%',
    position: 'absolute',
    width: '100%'
  }
});

const SIZE = 0.4;

const Circle =
  <NativeCircle
    cx={SIZE}
    cy={SIZE}
    fill='black'
    r={SIZE}
  />;
const Grid: FunctionComponent<GridProps> = (
  {
    children,
    scale = 1,
    xOffset: givenXOffset = 0,
    yOffset: givenYOffset = 0
  }: GridProps
) => {
  const diagramId = useDiagramId();
  const [
    givenGridX,
    givenGridY
  ] = useGrid() || [
    1,
    1
  ];

  const id = `lincle-grid-${diagramId}`;

  const gridX = useMemo(
    () => {
      return givenGridX * scale;
    },
    [
      givenGridX,
      scale
    ]
  );

  const gridY = useMemo(
    () => {
      return givenGridY * scale;
    },
    [
      givenGridY,
      scale
    ]
  );

  const xOffset = useMemo(
    () => {
      return givenXOffset % gridX;
    },
    [
      givenXOffset,
      gridX
    ]
  );

  const yOffset = useMemo(
    () => {
      return givenYOffset % gridY;
    },
    [
      givenYOffset,
      gridY
    ]
  );

  return (
    <Svg
      style={styles.grid}
    >
      <Pattern
        height={gridY}
        id={id}
        patternUnits='userSpaceOnUse'
        width={gridX}
        x={xOffset}
        y={yOffset}
      >
        {children ?? Circle}
      </Pattern>
      <Rect
        fill={`url(#${id})`}
        height='100%'
        width='100%'
        x='0'
        y='0'
      />
    </Svg>
  );
};

Grid.displayName = 'LincleGrid';

export default Grid;
