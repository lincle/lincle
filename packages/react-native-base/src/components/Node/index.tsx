import {
  type NodeProps,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useEdgeSubscriber,
  useNodePositions
} from '../../shared';
import {
  forwardRef,
  type ForwardRefRenderFunction,
  useEffect,
  useMemo
} from 'react';
import {
  View,
  type ViewStyle
} from 'react-native';

const Node: ForwardRefRenderFunction<HTMLDivElement, NodeProps> = (
  {
    children,
    height: givenHeight,
    id: nodeId,
    shape: givenShape,
    style: {
      node: nodeStyle
    } = {},
    track = true,
    width: givenWidth,
    x: givenX = 0,
    y: givenY = 0,
    ...additionalProperties
  },
  ref
) => {
  const defaultHeight = useDefaultNodeHeight();
  const defaultShape = useDefaultShape();
  const defaultWidth = useDefaultNodeWidth();
  const edgeSubscriber = useEdgeSubscriber();
  const nodePositions = useNodePositions();

  const height = givenHeight ?? defaultHeight ?? 0;
  const shape = givenShape ?? defaultShape ?? 'oval';
  const width = givenWidth ?? defaultWidth ?? 0;

  /*
    necessary for those who wrap this with components like <Draggable />
    see the following:
    https://github.com/mzabriskie/react-draggable/issues/414
  */
  const {
    x,
    y
  } = useMemo(
    () => {
      if (!(givenX || givenY)) {
        // eslint-disable-next-line regexp/no-unused-capturing-group
        const getNumber = /(-?\d+(\.\d+)?)/gu;
        const transform = nodeStyle?.transform as string | undefined;
        const [
          translateX,
          translateY
        ] = transform ?
          transform.match(getNumber) ?? [
            undefined,
            undefined
          ] :
          [
            undefined,
            undefined
          ];
        const left = nodeStyle?.left ?? undefined;
        const top = nodeStyle?.top ?? undefined;

        return {
          x: Number(translateX) || Number(left) || 0,
          y: Number(translateY) || Number(top) || 0
        };
      }

      return {
        x: givenX,
        y: givenY
      };
    },
    [
      givenX,
      givenY,
      nodeStyle
    ]
  );

  useEffect(
    () => {
      nodePositions?.register({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });

      return (): void => {
        nodePositions?.unregister(nodeId);
      };
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );

  useEffect(
    () => {
      nodePositions?.update({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });
    },
    [
      height,
      nodeId,
      nodePositions,
      shape,
      width,
      x,
      y
    ]
  );

  useEffect(
    (): void => {
      edgeSubscriber?.update({
        height,
        id: nodeId,
        shape,
        width,
        x,
        y
      });
    },
    [
      edgeSubscriber,
      height,
      nodeId,
      shape,
      width,
      x,
      y
    ]
  );

  const adjustedStyle = useMemo<ViewStyle>(
    () => {
      return {
        /* stylelint-disable */
        ...nodeStyle,
        height,
        position: track ?
          'absolute' :
          'relative',
        transform: track ?
          // eslint-disable-next-line @stylistic/no-extra-parens
          (
            nodeStyle?.transform ?? `translate(${x}px, ${y}px)`
          ) :
          'translate(0px, 0px)',
        transformOrigin: '0 0',
        width
      };
    },
    [
      height,
      nodeStyle,
      track,
      width,
      x,
      y
    ]
  );

  return (
    <View
      data-testid={`node-${nodeId}`}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      ref={ref as any}
      role='none'
      style={adjustedStyle}
      {...additionalProperties}
    >
      {children}
    </View>
  );
};

Node.displayName = 'LincleNode';

const refNode = forwardRef(Node);

export {
  refNode as Node
};
