import {
  type EdgesProps as EdgesPartialProps,
  type EdgeProps as SharedEdgeProps,
  type NodeProps as SharedNodeProps,
  type PathProps as SharedPathProps
} from '@lincle/react-shared';
import {
  type ViewStyle
} from 'react-native';

export type EdgeProps = SharedEdgeProps & {
  style?: {
    edge?: ViewStyle;
  };
};

export type EdgesProps = EdgesPartialProps & {};

export type NodeProps = SharedNodeProps & {
  style?: {
    node?: ViewStyle;
  };
};

export type PathProps = SharedPathProps & {
  style?: ViewStyle;
};

export {
  type EdgeNodeProps,
  generateOrigins,
  type GraphProps,
  type GridProps,
  type Line,
  type NodesProps,
  Providers,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useDiagramId,
  useEdge,
  useEdges,
  useEdgeSubscriber,
  useGrid,
  useNodePositions,
  useRemoveEdge,
  useUpdateEdge
} from '@lincle/react-shared';
