import {
  type ConnectionsProps,
  Marker,
  useConnections
} from '../../../shared';
import {
  Path
} from '../../Edge';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  StyleSheet
} from 'react-native';
import {
  Defs,
  G,
  Svg
} from 'react-native-svg';

const {
  svgStyle
} = StyleSheet.create({
  svgStyle: {
    overflow: 'visible',
    pointerEvents: 'box-none',
    position: 'absolute'
  }
});

const Connections: FunctionComponent<ConnectionsProps> = ({
  scale = 1,
  translate = {
    x: 0,
    y: 0
  }
}) => {
  const connections = useConnections();

  const paths = useMemo(
    () => {
      return Object.keys(connections).map(
        (
          sourceId
        ) => {
          const {
            line,
            source,
            target
          } = connections[sourceId];

          return (
            <Path
              edgeId={sourceId + 'temp'}
              key={sourceId}
              line={line}
              source={source}
              target={target}
            />
          );
        }
      );
    },
    [
      connections
    ]
  );

  const translateXYZ = useMemo(
    () => {
      const tx = translate.x;
      const ty = translate.y;
      const tz = scale;

      return `translate(${tx}, ${ty}) scale(${tz})`;
    },
    [
      translate.x,
      translate.y,
      scale
    ]
  );

  return (
    <Svg
      style={svgStyle}
    >
      <G
        transform={translateXYZ}
      >
        <Defs>
          <Marker />
        </Defs>
        {paths}
      </G>
    </Svg>
  );
};

Connections.displayName = 'LincleInteractiveConnections';

export default Connections;
