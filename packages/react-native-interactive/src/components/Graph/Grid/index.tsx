import {
  Grid as GridBase
} from '../../../base';
import {
  type GridProps,
  useMode,
  useScale,
  useTranslate
} from '../../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  Circle,
  Line
} from 'react-native-svg';

const WIDTH = 0.4;

const Grid: FunctionComponent<GridProps> = ({
  children
}) => {
  const scale = useScale();
  const translate = useTranslate();
  const mode = useMode();

  const line = useMemo(
    () => {
      const t1 = 1 + scale;
      const t2 = 4 * scale;

      return (
        <Line
          stroke='black'
          strokeWidth={WIDTH}
          x1={t1}
          x2={t2}
          y1={t1}
          y2={t2}
        />
      );
    },
    [
      scale
    ]
  );

  const plus = useMemo(
    () => {
      const t1 = scale;
      const t2 = 4 * scale;

      return (
        <>
          <Line
            stroke='black'
            strokeWidth={WIDTH}
            x1={t1}
            x2={t2}
            y1={t2}
            y2={t2}
          />
          <Line
            stroke='black'
            strokeWidth={WIDTH}
            x1={t2}
            x2={t2}
            y1={t1}
            y2={t2}
          />
        </>
      );
    },
    [
      scale
    ]
  );

  const circle = useMemo(
    () => {
      const t2 = 4 * scale;

      return (
        <Circle
          cx={t2}
          cy={t2}
          fill='black'
          r={WIDTH}
        />
      );
    },
    [
      scale
    ]
  );

  const child = useMemo(
    // eslint-disable-next-line @typescript-eslint/promise-function-async
    () => {
      if (!children) {
        // eslint-disable-next-line @typescript-eslint/switch-exhaustiveness-check
        switch (mode) {
          case 'move': {
            return circle;
          }

          case 'pull': {
            return line;
          }

          case 'select': {
            return plus;
          }
        }
      }

      return children;
    },
    [
      children,
      circle,
      line,
      mode,
      plus
    ]
  );

  return (
    <GridBase
      scale={scale}
      xOffset={translate.x}
      yOffset={translate.y}
    >
      {child}
    </GridBase>
  );
};

Grid.displayName = 'LincleInteractiveGrid';

export default Grid;
