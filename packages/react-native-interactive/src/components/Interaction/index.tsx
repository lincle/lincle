import {
  type InteractionProps,
  useMode,
  useOnMode,
  useOnScale,
  useOnTranslateThrottle,
  useScale,
  useTranslate
} from '../../shared';
import {
  ReactNativeZoomableView,
  type ZoomableViewEvent
} from '@openspacelabs/react-native-zoomable-view';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useRef
} from 'react';
import {
  type GestureResponderEvent,
  type PanResponderGestureState,
  StyleSheet,
  View
} from 'react-native';

const CLICK_TIME = 500;

const useDoubleTap = () => {
  const timer = useRef<NodeJS.Timeout | null>(null);

  useEffect(
    () => {
      return () => {
        if (timer.current) {
          clearTimeout(timer.current);
          timer.current = null;
        }
      };
    },
    []
  );

  return useCallback(
    (
      callback: () => void,
      threshold = CLICK_TIME
    ) => {
      if (timer.current) {
        clearTimeout(timer.current);
        timer.current = null;

        // eslint-disable-next-line n/callback-return
        callback();
      } else {
        timer.current = setTimeout(
          () => {
            timer.current = null;
          },
          threshold
        );
      }
    },
    []
  );
};

const styles = StyleSheet.create({
  container: {
    bottom: 0,
    height: '100%',
    left: 0,
    overflow: 'visible',
    position: 'absolute',
    right: 0,
    top: 0,
    width: '100%'
  },
  zoomable: {
    overflow: 'visible'
  }
});

const Interaction: FunctionComponent<InteractionProps> = ({
  onPointerUp
}) => {
  const scale = useScale();
  const translate = useTranslate();
  const onScale = useOnScale();
  const onTranslateThrottle = useOnTranslateThrottle();
  const graphMode = useMode() ?? 'move';
  const onMode = useOnMode();
  const handleDoubleTap = useDoubleTap();

  const handleOnTransform = useCallback(
    (event: ZoomableViewEvent) => {
      const {
        offsetX,
        offsetY,
        originalHeight,
        originalWidth,
        zoomLevel
      } = event;

      onTranslateThrottle(
        originalWidth,
        originalHeight,
        offsetX,
        offsetY,
        zoomLevel
      );
    },
    [
      onTranslateThrottle
    ]
  );

  const handleZoom = useCallback(
    (
      event: GestureResponderEvent | null,
      gestureState: null | PanResponderGestureState,
      zoomableViewEventObject: ZoomableViewEvent
    ) => {
      const {
        offsetX,
        offsetY,
        originalHeight,
        originalWidth,
        zoomLevel
      } = zoomableViewEventObject;

      onTranslateThrottle(
        originalWidth,
        originalHeight,
        offsetX,
        offsetY,
        zoomLevel
      );

      if (onScale) {
        onScale(zoomLevel);
      }
    },
    [
      onScale,
      onTranslateThrottle
    ]
  );

  const handleTapEnd = useCallback(
    () => {
      if (onMode) {
        switch (graphMode) {
          case 'move': {
            handleDoubleTap(
              () => {
                onMode('pull');
              }
            );

            return;
          }

          case 'pull': {
            handleDoubleTap(
              () => {
                onMode('select');
              }
            );

            return;
          }

          case 'select': {
            handleDoubleTap(
              () => {
                onMode('move');
              }
            );
          }
        }
      }
    },
    [
      graphMode,
      handleDoubleTap,
      onMode
    ]
  );

  const handlePointerUp = useCallback(
    (
      event: GestureResponderEvent
    ) => {
      handleTapEnd();

      if (onPointerUp) {
        onPointerUp(event);
      }
    },
    [
      handleTapEnd,
      onPointerUp
    ]
  );

  return (
    <View
      style={styles.container}
    >
      <ReactNativeZoomableView
        bindToBorders={false}
        doubleTapDelay={1}
        doubleTapZoomToCenter={false}
        initialOffsetX={translate.x}
        initialOffsetY={translate.y}
        initialZoom={scale}
        maxZoom={1.5}
        minZoom={0.5}
        movementSensibility={1}
        onSingleTap={handlePointerUp}
        onTransform={handleOnTransform}
        onZoomAfter={handleZoom}
        panEnabled
        style={styles.zoomable}
        visualTouchFeedbackEnabled={false}
        zoomStep={0.5}
      />
    </View>
  );
};

Interaction.displayName = 'LincleInteractiveInteraction';

export default Interaction;
