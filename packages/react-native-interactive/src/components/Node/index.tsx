import {
  type DraggableData,
  type DraggableEvent,
  type NodeProps,
  type Position,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useMode,
  useModeGiven,
  useMove,
  useOnNodeDrag,
  useOnNodeEdgeDrop,
  useOnNodeSelect,
  useOnNodeStart,
  useOnNodeStop,
  usePull,
  useSnap
} from '../../shared';
import MoveNode from './MoveNode';
import PullNode from './PullNode';
import {
  type FunctionComponent,
  useCallback,
  useMemo,
  useRef
} from 'react';

const emptySnap = [
  1,
  1
];

const Node: FunctionComponent<NodeProps> = ({
  children,
  height: givenHeight,
  id,
  line: givenLine,
  mode: givenMode,
  move: givenMove,
  onDrag,
  onEdgeDrop: givenOnEdgeDrop,
  onSelect: givenOnSelect,
  onStart,
  onStop,
  pull: givenPull,
  shape: givenShape,
  style,
  width: givenWidth,
  x,
  y
}) => {
  const defaultHeight = useDefaultNodeHeight();
  const defaultLine = useDefaultLine();
  const defaultShape = useDefaultShape();
  const defaultWidth = useDefaultNodeWidth();

  const snap = useSnap();

  const [
    snapX,
    snapY
  ] = snap && Array.isArray(snap) ?
    snap :
    emptySnap;

  const graphMode = useMode();
  const graphGivenMode = useModeGiven();

  const mode = givenMode ?? graphGivenMode ?? graphMode;

  const graphMove = useMove();
  const graphPull = usePull();

  const onNodeDrag = useOnNodeDrag();
  const onNodeEdgeDrop = useOnNodeEdgeDrop();
  const onNodeSelect = useOnNodeSelect();
  const onNodeStart = useOnNodeStart();
  const onNodeStop = useOnNodeStop();

  const move = givenMove ?? graphMove;
  const pull = givenPull ?? graphPull;

  const onEdgeDrop = givenOnEdgeDrop ?? onNodeEdgeDrop;
  const onSelect = givenOnSelect ?? onNodeSelect;

  const pullPosition = useRef({
    x,
    y
  });

  const {
    height,
    line,
    shape,
    width
  } = useMemo(
    () => {
      return {
        height: givenHeight ?? defaultHeight,
        line: givenLine ?? defaultLine,
        shape: givenShape ?? defaultShape,
        width: givenWidth ?? defaultWidth
      };
    },
    [
      defaultHeight,
      defaultLine,
      defaultShape,
      defaultWidth,
      givenHeight,
      givenLine,
      givenShape,
      givenWidth
    ]
  );

  const handleOnStart = useCallback(
    (
      event: DraggableEvent
    ) => {
      if (onStart) {
        onStart(
          event
        );
      } else if (onNodeStart) {
        onNodeStart(
          event,
          id
        );
      }
    },
    [
      id,
      onNodeStart,
      onStart
    ]
  );

  const handleOnDrag = useCallback(
    (
      event: DraggableEvent,
      data: DraggableData
    ) => {
      if (onDrag) {
        onDrag(
          event,
          data
        );
      } else if (onNodeDrag) {
        onNodeDrag(
          event,
          id,
          data
        );
      }
    },
    [
      id,
      onDrag,
      onNodeDrag
    ]
  );

  const handleOnStop = useCallback(
    (
      event: DraggableEvent,
      data: DraggableData
    ) => {
      if (onStop) {
        onStop(
          event,
          data
        );
      } else if (onNodeStop) {
        onNodeStop(
          event,
          id,
          data
        );
      }
    },
    [
      id,
      onNodeStop,
      onStop
    ]
  );

  const handleMoveStart = useCallback(
    (
      event: DraggableEvent
    ) => {
      handleOnStart(event);
    },
    [
      handleOnStart
    ]
  );

  const handleMoveDrag = useCallback(
    (
      event: DraggableEvent,
      data: DraggableData,
      position: Position
    ) => {
      pullPosition.current = {
        x: position.x,
        y: position.y
      };

      handleOnDrag(
        event,
        data
      );
    },
    [
      handleOnDrag
    ]
  );

  const handleMoveStop = useCallback(
    (
      event: DraggableEvent,
      data: DraggableData
    ) => {
      handleOnStop(
        event,
        data
      );
    },
    [
      handleOnStop
    ]
  );

  const pullNode = useMemo(
    () => {
      return mode === 'pull' && pull ?

        <PullNode
          height={height}
          line={line}
          mode={mode}
          onEdgeDrop={onEdgeDrop}
          shape={shape}
          sourceId={id}
          style={style}
          width={width}
          x={pullPosition.current.x}
          y={pullPosition.current.y}
        /> :
        null;
    },
    [ // eslint-disable-line react-hooks/exhaustive-deps
      mode
    ]
  );

  const snapper = useMemo(
    () => {
      return {
        x: Math.ceil((x ?? 0) / snapX) * snapX,
        y: Math.ceil((y ?? 0) / snapY) * snapY
      };
    },
    [
      snapX,
      snapY,
      x,
      y
    ]
  );

  return (
    <>
      <MoveNode
        disabled={
          mode === 'pull' ?
            true :
            !move
        }
        height={height}
        id={id}
        mode={mode}
        onDrag={handleMoveDrag}
        onSelect={onSelect}
        onStart={handleMoveStart}
        onStop={handleMoveStop}
        shape={shape}
        snap={snap}
        style={style}
        width={width}
        x={snapper.x}
        y={snapper.y}
      >
        {children}
      </MoveNode>
      {pullNode}
    </>
  );
};

Node.displayName = 'LincleInteractiveNode';

export {
  Node
};

export {
  default as MoveNode
} from './MoveNode';
export {
  default as PullNode
} from './PullNode';
