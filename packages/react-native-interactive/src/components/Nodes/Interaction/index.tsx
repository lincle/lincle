import {
  type InteractionProps,
  useScale,
  useTranslate
} from '../../../shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backfaceVisibility: 'hidden',
    height: '100%',
    overflow: 'visible',
    pointerEvents: 'box-none',
    transformOrigin: '0 0'
    // willChange: 'transform'
  }
});

const Interaction: FunctionComponent<InteractionProps> = ({
  children
}) => {
  const scale = useScale();
  const translate = useTranslate();

  const style = useMemo(
    () => {
      const {
        x,
        y
      } = translate;

      return {
        ...styles.container,
        /* stylelint-disable */
        transform: `translateX(${x}px) translateY(${y}px) scale(${scale})`
      };
    },
    [
      scale,
      translate
    ]
  );

  return (
    <View
      style={style}
    >
      {children}
    </View>
  );
};

Interaction.displayName = 'LincleInteractiveInteraction';

export default Interaction;
