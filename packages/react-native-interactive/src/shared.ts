import {
  type EdgeControlProps,
  type GraphProps as InteractiveGraphProps,
  type Position,
  type MoveNodeProps as SharedMoveNodeProps,
  type NodeProps as SharedNodeProps,
  type PullNodeProps as SharedPullNodeProps,
  type PullProps as SharedPullProps
} from '@lincle/react-interactive-shared';
import {
  type EdgeProps as BaseEdgeProps,
  type EdgesProps as BaseEdgesProps,
  type NodeProps as BaseNodeProps,
  type NodesProps as BaseNodesProps
} from '@lincle/react-native-base';
import {
  type PropsWithChildren
} from 'react';
import {
  type GestureResponderEvent,
  type PanResponderGestureState,
  type ViewStyle
} from 'react-native';

export type DraggableData = PanResponderGestureState;
export type DraggableEvent = GestureResponderEvent;

export type EdgeProps = BaseEdgeProps;

export type EdgesProps = BaseEdgesProps;

export type GraphNodeControlProps = {
  onNodeDrag?: (
    event: DraggableEvent,
    nodeId: number | string,
    data: DraggableData
  ) => void;
  onNodeSelect?: (
    nodeId: number | string
  ) => void;
  onNodeStart?: (
    event: DraggableEvent,
    nodeId: number | string
  ) => void;
  onNodeStop?: (
    event: DraggableEvent,
    nodeId: number | string,
    data: DraggableData
  ) => void;
};

export type GraphProps = GraphNodeControlProps &
Omit<
  InteractiveGraphProps,
'onNodeDrag' |
'onNodeSelect' |
'onNodeStart' |
'onNodeStop'
>;

export type InteractionControlProps = {
  onPointerUp?: (
    event: GestureResponderEvent
  ) => void;
};

export type InteractionProps = PropsWithChildren<InteractionControlProps>;

export type MoveNodeProps = BaseNodeProps & Omit<NodeProps, 'onDrag' | 'onSelect'> & SharedMoveNodeProps & {
  readonly onDrag?: (
    event: GestureResponderEvent,
    data: DraggableData,
    position: Position
  ) => void;
};

export type NodeContextProps = EdgeControlProps & GraphNodeControlProps;

export type NodeControlProps = {
  onDrag?: (
    event: DraggableEvent,
    data: DraggableData
  ) => void;
  onEdgeDrop?: (
    event: DraggableEvent,
    sourceId: number | string,
    targetId?: number | string
  ) => void;
  onSelect?: () => void;
  onStart?: (
    event: DraggableEvent
  ) => void;
  onStop?: (
    event: DraggableEvent,
    data: DraggableData
  ) => void;
};

export type NodeProps = NodeControlProps & Omit<BaseNodeProps, 'ref'> & SharedNodeProps;

export type NodesProps = BaseNodesProps;

export type PullNodeProps = Omit<NodeProps, 'id'> & SharedPullNodeProps & {
  style?: {
    node?: ViewStyle;
    pull?: ViewStyle;
  };
};

export type PullProps = Omit<EdgeProps, 'children' | 'id' | 'path' | 'targetId'> &
Omit<NodeProps, 'children' | 'id'> & SharedPullProps & {
  style?: {
    pull?: ViewStyle;
  };
};

export {
  type Connection,
  type Connections,
  type ConnectionsProps,
  type EdgeNodeProps,
  type GridProps,
  type ModeType,
  type Position,
  Providers,
  useConnection,
  useConnections,
  useDefaultLine,
  useDefaultNodeHeight,
  useDefaultNodeWidth,
  useDefaultShape,
  useDiagramId,
  useMode,
  useModeGiven,
  useMove,
  useNodePositions,
  useOnMode,
  useOnNodeDrag,
  useOnNodeEdgeDrop,
  useOnNodeSelect,
  useOnNodeStart,
  useOnNodeStop,
  useOnScale,
  useOnTranslateThrottle,
  usePull,
  useScale,
  useSetConnection,
  useSnap,
  useTranslate
} from '@lincle/react-interactive-shared';
export {
  Graph as GraphBase,
  Marker,
  useUpdateEdge
} from '@lincle/react-native-base';
