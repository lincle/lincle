import MiniMapPlain from '../MiniMapPlain';
import {
  type MiniMapProps
} from '../types';
import {
  type FunctionComponent,
  memo,
  type ReactElement,
  useMemo
} from 'react';
import {
  StyleSheet,
  useWindowDimensions
} from 'react-native';

const {
  LincleMinimapContainerStyle
} = StyleSheet.create({
  LincleMinimapContainerStyle: {
    alignItems: 'center',
    bottom: 16,
    justifyContent: 'center',
    overflow: 'hidden',
    position: 'absolute',
    right: 16
  },
  LincleMinimapSizeStyle: {
    bottom: 0,
    left: 0,
    pointerEvents: 'box-none',
    position: 'absolute',
    right: 0,
    top: 0
  }
});

const MiniMap: FunctionComponent<MiniMapProps> = ({
  gutter,
  height,
  node,
  style: givenStyle,
  width
}): ReactElement => {
  const {
    height: lincleHeight,
    width: lincleWidth
  } = useWindowDimensions();

  const style = useMemo(
    () => {
      return {
        ...LincleMinimapContainerStyle,
        ...givenStyle
      };
    },
    [
      givenStyle
    ]
  );

  return (
    <MiniMapPlain
      gutter={gutter}
      height={height}
      lincleHeight={lincleHeight}
      lincleWidth={lincleWidth}
      node={node}
      style={style}
      width={width}
    />
  );
};

MiniMap.displayName = 'LincleMiniMap';

export default memo(MiniMap);
