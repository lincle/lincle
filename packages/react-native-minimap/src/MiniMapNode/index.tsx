import {
  type MiniMapNodeProps
} from '../types';
import Ellipse from './Ellipse';
import Rectangle from './Rectangle';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const MiniMapNode: FunctionComponent<MiniMapNodeProps> = ({
  height,
  shape,
  width,
  x,
  y
}) => {
  const daShape = useMemo(
    () => {
      return (
        shape === 'oval' ?

          <Ellipse
            height={height}
            width={width}
            x={x}
            y={y}
          /> :

          <Rectangle
            height={height}
            width={width}
            x={x}
            y={y}
          />

      );
    },
    [
      height,
      shape,
      width,
      x,
      y
    ]
  );

  return daShape;
};

MiniMapNode.displayName = 'LincleMiniMapNode';

export default MiniMapNode;
