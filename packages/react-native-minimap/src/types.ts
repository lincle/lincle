import {
  type Dimensions,
  type Shape
} from '@lincle/react-shared';
import {
  type ReactElement
} from 'react';
import {
  type ViewStyle
} from 'react-native';

export type MiniMapNodeProps = {
  readonly height: number;
  readonly shape?: Shape;
  readonly width: number;
  readonly x: number;
  readonly y: number;
};

export type MiniMapPlainProps = MiniMapProps & {
  readonly lincleHeight: number;
  readonly lincleWidth: number;
  readonly style?: ViewStyle;
};

export type MiniMapProps = {
  readonly gutter?: number;
  readonly height?: number;
  readonly node?: (
    dimensions: Dimensions
  ) => ReactElement;
  readonly style?: ViewStyle;
  readonly width?: number;
};

export type MiniMapSvgProps = MiniMapProps & {
  readonly lincleHeight: number;
  readonly lincleWidth: number;
  readonly style?: ViewStyle;
};
