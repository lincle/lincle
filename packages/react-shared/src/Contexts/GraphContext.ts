import defaultSettings from '../defaultSettings';
import {
  type GridType,
  type Line,
  type Shape
} from '../types';
import type EdgeSubscriber from '../utils/EdgeSubscriber';
import type NodePositions from '../utils/NodePositions';
import {
  createContext
} from 'react';

const GraphContext = createContext<GraphContextProps>({
  edgeFrequency: defaultSettings.edgeFrequency,
  grid: defaultSettings.grid,
  line: defaultSettings.line,
  nodeFrequency: defaultSettings.nodeFrequency,
  nodeHeight: defaultSettings.height,
  nodeWidth: defaultSettings.width,
  shape: defaultSettings.shape
});

GraphContext.displayName = 'LincleGraphContext';

export {
  GraphContext
};

export type GraphContextProps = {
  edgeFrequency: number;
  edgeSubscriber?: EdgeSubscriber;
  grid: false | GridType;
  id?: number | string;
  line: Line;
  nodeFrequency: number;
  nodeHeight: number;
  nodePositions?: NodePositions;
  nodeWidth: number;
  shape: Shape;
};

