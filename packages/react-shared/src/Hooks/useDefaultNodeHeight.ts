import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDefaultNodeHeight = () => {
  const nodeHeight = useContext(GraphContext).nodeHeight;

  return nodeHeight;
};

export {
  useDefaultNodeHeight
};
