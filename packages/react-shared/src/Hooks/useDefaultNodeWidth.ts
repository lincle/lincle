import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDefaultNodeWidth = () => {
  const width = useContext(GraphContext).nodeWidth;

  return width;
};

export {
  useDefaultNodeWidth
};
