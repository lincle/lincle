import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDefaultShape = () => {
  const shape = useContext(GraphContext).shape;

  return shape;
};

export {
  useDefaultShape
};
