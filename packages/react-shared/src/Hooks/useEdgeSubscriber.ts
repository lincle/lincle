import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useEdgeSubscriber = () => {
  const {
    edgeSubscriber
  } = useContext(GraphContext);

  return edgeSubscriber;
};

export {
  useEdgeSubscriber
};
