import {
  GraphContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useGrid = () => {
  const {
    grid
  } = useContext(GraphContext);

  return grid;
};

export {
  useGrid
};
