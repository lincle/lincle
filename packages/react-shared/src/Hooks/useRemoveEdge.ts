import {
  EdgesContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useRemoveEdge = () => {
  const {
    removeEdge
  } = useContext(EdgesContext);

  return removeEdge;
};

export {
  useRemoveEdge
};
