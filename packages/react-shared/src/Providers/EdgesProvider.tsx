import {
  type Edges,
  EdgesContext,
  type RemoveEdge,
  type UpdateEdge
} from '../Contexts';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo,
  useState
} from 'react';

const EdgesProvider: FunctionComponent<EdgesProviderProps> = ({
  children
}) => {
  const [
    edges,
    setEdges
  ] = useState<Edges>({});

  const removeEdge = useCallback<RemoveEdge>(
    (
      edgeId: number | string
    ) => {
      setEdges(
        (
          currentEdges
        ) => {
          const {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            [edgeId]: omit,
            ...rest
          } = currentEdges;

          return rest;
        }
      );
    },
    []
  );

  const updateEdge = useCallback<UpdateEdge>(
    (
      edgeId,
      props
    ) => {
      setEdges(
        (
          currentEdges
        ) => {
          const edge = currentEdges[edgeId];

          return {
            ...currentEdges,
            [edgeId]: {
              ...edge,
              ...props
            }
          };
        }
      );
    },
    []
  );

  const value = useMemo(
    () => {
      return {
        edges,
        removeEdge,
        updateEdge
      };
    },
    [
      edges,
      removeEdge,
      updateEdge
    ]
  );

  return (
    <EdgesContext.Provider
      value={value}
    >
      {children}
    </EdgesContext.Provider>
  );
};

EdgesProvider.displayName = 'LincleEdgesProvider';

export {
  EdgesProvider
};

export type EdgesProviderProps = PropsWithChildren<object>;
