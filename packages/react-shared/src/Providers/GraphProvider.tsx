import {
  GraphContext
} from '../Contexts';
import {
  type GraphContextProps
} from '../Contexts/GraphContext';
import defaultSettings from '../defaultSettings';
import EdgeSubscriber from '../utils/EdgeSubscriber';
import NodePositions from '../utils/NodePositions';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';

const GraphProvider: FunctionComponent<GraphProviderProps> = ({
  children,
  edgeFrequency = defaultSettings.edgeFrequency,
  grid = defaultSettings.grid,
  id,
  line = defaultSettings.line,
  nodeFrequency = defaultSettings.nodeFrequency,
  nodeHeight = defaultSettings.height,
  nodeWidth = defaultSettings.width,
  shape = defaultSettings.shape
}) => {
  const permanent = useMemo(
    () => {
      return {
        edgeSubscriber: new EdgeSubscriber(edgeFrequency),
        id,
        nodePositions: new NodePositions(nodeFrequency)
      };
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );

  const provides = useMemo(
    () => {
      return {
        ...permanent,
        edgeFrequency,
        grid,
        line,
        nodeFrequency,
        nodeHeight,
        nodeWidth,
        shape
      };
    },
    [
      edgeFrequency,
      grid,
      line,
      nodeFrequency,
      nodeHeight,
      nodeWidth,
      permanent,
      shape
    ]
  );

  return (
    <GraphContext.Provider
      value={provides}
    >
      {children}
    </GraphContext.Provider>
  );
};

GraphProvider.displayName = 'LincleGraphProvider';

export type GraphProviderProps = PropsWithChildren<
Omit<
  GraphContextProps,
'edgeSubscriber' |
'id' |
'nodePositions'
> &
Required<
  Pick<
    GraphContextProps,
    'id'
  >
>
>;

export {
  GraphProvider
};
