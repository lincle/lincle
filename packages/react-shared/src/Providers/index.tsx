import defaultSettings from '../defaultSettings';
import {
  EdgesProvider,
  GraphProvider,
  type GraphProviderProps
} from './Providers';
import {
  type FunctionComponent
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  edgeFrequency = defaultSettings.edgeFrequency,
  grid = defaultSettings.grid,
  id,
  line = defaultSettings.line,
  nodeFrequency = defaultSettings.nodeFrequency,
  nodeHeight = defaultSettings.height,
  nodeWidth = defaultSettings.width,
  shape = defaultSettings.shape
}) => {
  return (
    <GraphProvider
      edgeFrequency={edgeFrequency}
      grid={grid}
      id={id}
      line={line}
      nodeFrequency={nodeFrequency}
      nodeHeight={nodeHeight}
      nodeWidth={nodeWidth}
      shape={shape}
    >
      <EdgesProvider>
        {children}
      </EdgesProvider>
    </GraphProvider>
  );
};

Providers.displayName = 'LincleProviders';

export {
  Providers
};

export type ProvidersProps = Partial<
  Omit<GraphProviderProps, 'id'>
> &
Pick<GraphProviderProps, 'id'>;
