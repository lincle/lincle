import {
  type Line,
  type Shape
} from './types';

export default {
  edgeFrequency: 16,
  grid: [
    16,
    16
  ] as [number, number],
  height: 50,
  line: 'step' as Line,
  nodeFrequency: 16,
  shape: 'oval' as Shape,
  showGrid: true,
  width: 50
};
