import defaultSettings from '../defaultSettings';
import {
  type EdgeNodeProps
} from '../types';
import throttle from 'lodash.throttle';

const FREQUENCY = defaultSettings.edgeFrequency;

/**
 * Edges subscribe here for their source and target node position updates.
 */
class EdgeSubscriber {
  private frequency: number;

  private nodes: {
    [key: string]: {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      [key: string]: (...args: any) => void;
    };
  } = {};

  /* temporary cache only for newly created edges */
  private propsCache: {
    [key: string]: EdgeNodeProps;
  } = {};

  private throttleUpdates: {
    [key: string]: (
      props: EdgeNodeProps
    ) => void;
  } = {};

  public constructor (
    frequency = FREQUENCY
  ) {
    this.frequency = frequency;
  }

  public getFrequency = (): number => {
    return this.frequency;
  };

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-parameters
  public register = <T>(
    edgeId: number | string,
    nodeId: number | string,
  // eslint-disable-next-line unicorn/prevent-abbreviations
    updateFn: (update: T) => void
  ): EdgeNodeProps => {
    if (!this.nodes[nodeId]) {
      this.nodes[nodeId] = {};
    }

    this.nodes[nodeId][edgeId] = updateFn;

    return this.propsCache[nodeId];
  };

  public setFrequency = (
    frequency: number
  ): number => {
    this.frequency = frequency;

    return this.frequency;
  };

  public unregister = (
    edgeId: number | string,
    nodeId: number | string
  ): void => {
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.nodes[nodeId][edgeId];
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.throttleUpdates[nodeId];
  };

  public update = (
    props: EdgeNodeProps
  ): void => {
    const {
      id
    } = props;

    if (!this.throttleUpdates[id]) {
      this.throttleUpdates[id] = throttle(
        this.updateEdges,
        this.frequency,
        {
          trailing: true
        }
      );
    }

    this.throttleUpdates[id](props);
  };

  private readonly updateEdges = (
    props: EdgeNodeProps
  ): void => {
    if (this.nodes[props.id]) {
      for (
        const [
          // eslint-disable-next-line @typescript-eslint/naming-convention,@typescript-eslint/no-unused-vars
          _edgeId,
          update
        ] of Object.entries(this.nodes[props.id])
      ) {
        update(props);
      }
    }

    this.propsCache[props.id] = props;
  };
}

export default EdgeSubscriber;
