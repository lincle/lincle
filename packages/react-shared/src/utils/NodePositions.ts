import defaultSettings from '../defaultSettings';
import {
  type Dimensions,
  type NodesDimensions,
  type Size
} from '../types';
import debounce from 'lodash.debounce';

const FREQUENCY = defaultSettings.nodeFrequency;
const MAX_WAIT = defaultSettings.nodeFrequency;

/**
 * Keeps track of node positions for future tools such as connecting edges,
 * overview map, etc.
 */
class NodePositions {
  private bottom: Dimensions | undefined;

  private debounceUpdates: {
    [key: string]: (
      props: Dimensions
    ) => void;
  } = {};

  private frequency: number;

  private left: Dimensions | undefined;

  private nodes: NodesDimensions = {};

  private right: Dimensions | undefined;

  private subscriptions: {
    [key: string]: (
      size: Size,
      nodes: NodesDimensions
    ) => void;
  } = {};

  private top: Dimensions | undefined;

  public constructor (
    frequency = FREQUENCY
  ) {
    this.frequency = frequency;
  }

  public getFrequency = (): number => {
    return this.frequency;
  };

  public match = async (
    x: number,
    y: number
  ): Promise<number | string | undefined> => {
    const ids = await Promise.resolve(
      Object.keys(this.nodes).find(
        (id): boolean => {
          const node = this.nodes[id];

          return x > node.x &&
            x < node.x + node.width &&
            y > node.y &&
            y < node.y + node.height;
        }
      )
    );

    return ids ?? undefined;
  };

  public register = (
    node: Dimensions,
    {
      maxWait = MAX_WAIT
    } = {}
  ): void => {
    this.nodes[node.id] = node;

    this.debounceUpdates[node.id] = debounce(
      this.privateUpdate,
      this.frequency,
      {
        maxWait,
        trailing: true
      }
    );
  };

  public setFrequency = (
    frequency: number
  ): number => {
    this.frequency = frequency;

    return this.frequency;
  };

  public size = (): Size => {
    return {
      bottom: (this.bottom?.y ?? 0) + (this.bottom?.height ?? 0),

      height:
        (this.bottom?.y ?? 0) +
        (this.bottom?.height ?? 0) -
       (this.top?.y ?? 0),
      left: this.left?.x ?? 0,
      right: (this.right?.x ?? 0) + (this.right?.width ?? 0),
      top: this.top?.y ?? 0,

      width:
        (this.right?.x ?? 0) +
        (this.right?.width ?? 0) -
       (this.left?.x ?? 0)
    };
  };

  public subscribe = (
    id: string,
    callback: (
      size?: Size,
      nodes?: NodesDimensions
    ) => void
  ): void => {
    this.subscriptions[id] = callback;

    callback(
      this.size(),
      this.nodes
    );
  };

  public unregister = (
    id: number | string
  ): void => {
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.nodes[id];

    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.debounceUpdates[id];

    if (
      this.top?.id === id
    ) {
      this.top = undefined;

      for (const nodeId of Object.keys(this.nodes)) {
        if (
          !this.top ||
            this.nodes[nodeId].y < this.top.y
        ) {
          this.top = this.nodes[nodeId];
        }
      }
    }

    if (
      this.right?.id === id
    ) {
      this.right = undefined;

      for (const nodeId of Object.keys(this.nodes)) {
        if (
          !this.right ||
            this.nodes[nodeId].x + this.nodes[nodeId].height > this.right.x + this.right.width
        ) {
          this.right = this.nodes[nodeId];
        }
      }
    }

    if (
      this.bottom?.id === id
    ) {
      this.bottom = undefined;

      for (const nodeId of Object.keys(this.nodes)) {
        if (
          !this.bottom ||
            this.nodes[nodeId].y + this.nodes[nodeId].height > this.bottom.y + this.bottom.height
        ) {
          this.bottom = this.nodes[nodeId];
        }
      }
    }

    if (
      this.left?.id === id
    ) {
      this.left = undefined;

      for (const nodeId of Object.keys(this.nodes)) {
        if (
          !this.left ||
            this.nodes[nodeId].x < this.left.x
        ) {
          this.left = this.nodes[nodeId];
        }
      }
    }

    this.notify();
  };

  public unsubscribe = (
    id: string
  ): void => {
    // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
    delete this.subscriptions[id];
  };

  public update = (
    node: Dimensions
  ): void => {
    this.debounceUpdates[node.id](node);
  };

  private readonly getBottom = (
    node: Dimensions
  ): void => {
    const nodeY = node.y + node.height;
    const bottomNodeY = this.bottom ?
      this.bottom.y + this.bottom.height :
      undefined;

    if (
      this.bottom &&
        this.bottom.id === node.id
    ) {
      if (
        !bottomNodeY ||
        nodeY < bottomNodeY
      ) {
        this.bottom = node;

        for (const id of Object.keys(this.nodes)) {
          // recalculate since this.bottom may change
          // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
          // @ts-ignore
          if (this.nodes[id].y + this.nodes[id].height > this.bottom.y + this.bottom.height) {
            this.bottom = this.nodes[id];
          }
        }
      } else {
        this.bottom = node;
      }
    } else if (
      !this.bottom ||
      bottomNodeY &&
      nodeY > bottomNodeY
    ) {
      this.bottom = node;
    }
  };

  private readonly getLeft = (
    node: Dimensions
  ): void => {
    if (
      this.left &&
        this.left.id === node.id
    ) {
      if (node.x > this.left.x) {
        this.left = node;

        for (const id of Object.keys(this.nodes)) {
          // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
          // @ts-ignore
          if (this.nodes[id].x < this.left.x) {
            this.left = this.nodes[id];
          }
        }
      } else {
        this.left = node;
      }
    } else if (
      !this.left ||
      node.x < this.left.x
    ) {
      this.left = node;
    }
  };

  private readonly getRight = (
    node: Dimensions
  ): void => {
    const nodeX = node.x + node.width;
    const rightNodeX = this.right ?
      this.right.x + this.right.width :
      undefined;

    if (
      this.right &&
        this.right.id === node.id
    ) {
      if (
        !rightNodeX ||
        nodeX < rightNodeX
      ) {
        this.right = node;

        for (const id of Object.keys(this.nodes)) {
          // recalculate since this.right may change
          // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
          // @ts-ignore
          if (this.nodes[id].x + this.nodes[id].width > this.right.x + this.right.width) {
            this.right = this.nodes[id];
          }
        }
      } else {
        this.right = node;
      }
    } else if (
      !this.right ||
      rightNodeX &&
      nodeX > rightNodeX
    ) {
      this.right = node;
    }
  };

  private readonly getSize = (
    node: Dimensions
  ): void => {
    this.getTop(node);
    this.getRight(node);
    this.getBottom(node);
    this.getLeft(node);
  };

  private readonly getTop = (
    node: Dimensions
  ): void => {
    if (
      this.top &&
        this.top.id === node.id
    ) {
      if (node.y > this.top.y) {
        this.top = node;

        for (const id of Object.keys(this.nodes)) {
          if (this.top && this.nodes[id].y < this.top.y) {
            this.top = this.nodes[id];
          }
        }
      } else {
        this.top = node;
      }
    } else if (
      !this.top ||
      node.y < this.top.y
    ) {
      this.top = node;
    }
  };

  private readonly notify = (): void => {
    for (const [
      // eslint-disable-next-line @typescript-eslint/naming-convention,@typescript-eslint/no-unused-vars
      _id,
      update
    ] of Object.entries(this.subscriptions)) {
      update(
        this.size(),
        this.nodes
      );
    }
  };

  private readonly privateUpdate = (
    node: Dimensions
  ): void => {
    this.register(node);

    this.getSize(node);

    this.notify();
  };
}

export default NodePositions;
