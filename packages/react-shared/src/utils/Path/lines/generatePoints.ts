import {
  type Points
} from './types';

export default (
  source: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  },
  target: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  }
): Points => {
  const sx = Math.round(source.x);
  const sy = Math.round(source.y);
  const tx = Math.round(target.x);
  const ty = Math.round(target.y);

  const halfTargetWidth = Math.round(target.width / 2);
  const halfTargetHeight = Math.round(target.height / 2);
  const halfSourceWidth = Math.round(source.width / 2);
  const halfSourceHeight = Math.round(source.height / 2);

  const sourceAngle = Math.atan2(
    tx + halfTargetWidth - (sx + halfSourceWidth),
    ty + halfTargetHeight - (sy + halfSourceHeight)
  );
  const targetAngle = Math.atan2(
    sx + halfSourceWidth - (tx + halfTargetWidth),
    sy + halfSourceHeight - (ty + halfTargetHeight)
  );

  const sourceEdgeX = Math.sin(sourceAngle) * halfSourceWidth + halfSourceWidth;
  const sourceEdgeY = Math.cos(sourceAngle) * halfSourceHeight + halfSourceHeight;
  const targetEdgeX = Math.sin(targetAngle) * halfTargetWidth + halfTargetWidth;
  const targetEdgeY = Math.cos(targetAngle) * halfTargetHeight + halfTargetHeight;

  const sourceDX = sx + sourceEdgeX - tx - targetEdgeX;
  const sourceDY = sy + sourceEdgeY - ty - targetEdgeY;
  const sourceROR = sourceDX / sourceDY;

  let orientation: 'horizontal' | 'vertical';
  let targetPoints = {
    x: sourceEdgeX,
    y: sourceEdgeY
  };

  if (sourceROR < 1 && sourceROR > -1) {
    orientation = 'vertical';
    if (sourceDY > 0) {
      // top
      targetPoints = {
        x: sx + sourceEdgeX,
        y: sy + (source.shape === 'oval' ?
          sourceEdgeY :
          0)
      };
    } else {
      // bottom
      targetPoints = {
        x: sx + sourceEdgeX,
        y: sy + (source.shape === 'oval' ?
          sourceEdgeY :
          source.height)
      };
    }
  } else {
    orientation = 'horizontal';
    if (sourceDX > 0) {
      // left
      targetPoints = {
        x: sx + (source.shape === 'oval' ?
          sourceEdgeX :
          0),
        y: sy + sourceEdgeY
      };
    } else {
      // left
      targetPoints = {
        x: sx + (source.shape === 'oval' ?
          sourceEdgeX :
          source.width),
        y: sy + sourceEdgeY
      };
    }
  }

  const targetDX = tx - targetEdgeX - sx - sourceEdgeX;
  const targetDY = ty - targetEdgeY - sy - sourceEdgeY;

  let sourcePoints = {
    x: targetEdgeX,
    y: targetEdgeY
  };

  if (orientation === 'vertical') {
    if (targetDY > 0) {
      // top
      sourcePoints = {
        x: tx + targetEdgeX,
        y: ty + (target.shape === 'oval' ?
          targetEdgeY :
          0)
      };
    } else {
      // bottom
      sourcePoints = {
        x: tx + targetEdgeX,
        y: ty + (target.shape === 'oval' ?
          targetEdgeY :
          target.height)
      };
    }
  } else {
    // eslint-disable-next-line no-lonely-if
    if (targetDX > 0) {
      // left
      sourcePoints = {
        x: tx + (target.shape === 'oval' ?
          targetEdgeX :
          0),
        y: ty + targetEdgeY
      };
    } else {
      // left
      sourcePoints = {
        x: tx + (target.shape === 'oval' ?
          targetEdgeX :
          target.width),
        y: ty + targetEdgeY
      };
    }
  }

  return {
    orientation,
    source: sourcePoints,
    target: targetPoints
  };
};
