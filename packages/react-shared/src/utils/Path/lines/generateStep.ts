/*
  eslint-disable
  id-length
*/

import generatePoints from './generatePoints';
import {
  type Coordinates,
  type Line
} from './types';

export const stepHorizontal = (
  rx1: number,
  ry1: number,
  rx2: number,
  ry2: number,
  center?: boolean
): Line => {
  const x1 = Math.round(rx1);
  const y1 = Math.round(ry1);
  const x2 = Math.round(rx2);
  const y2 = Math.round(ry2);

  const d = [];
  const mx = Math.round(x1 + (x2 - x1) / 2);
  const my = Math.round(y1 + (y2 - y1) / 2);
  const cx = Math.round((x2 + x1) / 2);
  const cy = my;

  d.push(
    'M',
    x1,
    y1
  );
  d.push(
    'L',
    mx,
    y1
  );
  d.push(
    'L',
    mx,
    y2
  );
  d.push(
    'L',
    x2,
    y2
  );

  return {
    center: center ?
      {
        x: cx,
        y: cy
      } :
      undefined,
    path: d.join(' ')
  };
};

export const stepVertical = (
  rx1: number,
  ry1: number,
  rx2: number,
  ry2: number,
  center?: boolean
): Line => {
  const x1 = Math.round(rx1);
  const y1 = Math.round(ry1);
  const x2 = Math.round(rx2);
  const y2 = Math.round(ry2);

  const d = [];
  const mx = Math.round(x1 + (x2 - x1) / 2);
  const my = Math.round(y1 + (y2 - y1) / 2);
  const cx = mx;
  const cy = Math.round((y2 + y1) / 2);

  d.push(
    'M',
    x1,
    y1
  );
  d.push(
    'L',
    x1,
    my
  );
  d.push(
    'L',
    x2,
    my
  );
  d.push(
    'L',
    x2,
    y2
  );

  return {
    center: center ?
      {
        x: cx,
        y: cy
      } :
      undefined,
    path: d.join(' ')
  };
};

export default (
  source: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  },
  target: {
    height: number;
    shape: 'oval' | 'rectangle';
    width: number;
    x: number;
    y: number;
  },
  center?: boolean
): Coordinates => {
  const {
    orientation,
    source: sourcePoints,
    target: targetPoints
  } = generatePoints(
    source,
    target
  );

  return {
    ...orientation === 'horizontal' ?
      stepHorizontal(
        sourcePoints.x,
        sourcePoints.y,
        targetPoints.x,
        targetPoints.y,
        center
      ) :
      stepVertical(
        sourcePoints.x,
        sourcePoints.y,
        targetPoints.x,
        targetPoints.y,
        center
      ),
    source: sourcePoints,
    target: targetPoints
  };
};
