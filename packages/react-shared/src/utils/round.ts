const round = (
  coordinate: number,
  grid: number
): number => {
  if (grid === 1) {
    return Math.round(coordinate);
  }

  return Math.round(coordinate / grid) * grid;
};

export default round;
