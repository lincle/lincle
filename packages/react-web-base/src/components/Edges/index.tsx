import {
  type EdgesProps
} from '../../shared';
import Marker from './Marker';
import Paths from './Paths';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const dashClass = (dash?: boolean) => {
  if (dash === true) {
    return 'lincle-base-edge-dash-all';
  } else if (dash === false) {
    return 'lincle-base-edge-dash-none';
  } else {
    return '';
  }
};

const Edges: FunctionComponent<EdgesProps> = (
  {
    children,
    className: givenClassName = '',
    dash,
    scale = 1,
    style: givenStyle,
    translate = {
      x: 0,
      y: 0
    }
  }: EdgesProps
) => {
  const style = useMemo(
    () => {
      const tx = translate.x;
      const ty = translate.y;
      const tz = scale;

      const transform = `translateX(${tx}px) translateY(${ty}px) scale(${tz})`;

      return {
        ...givenStyle,
        transform,
        transformOrigin: 'top left'
      };
    },
    [
      translate.x,
      translate.y,
      scale,
      givenStyle
    ]
  );

  const className = useMemo(
    () => {
      return `lincle-base-edges ${givenClassName} ${dashClass(dash)}`;
    },
    [
      dash,
      givenClassName
    ]
  );

  return (
    <>
      <svg
        className={className}
        style={style}
      >
        <defs>
          <Marker />
        </defs>
        <Paths />
      </svg>
      <div
        className={className}
        style={style}
      >
        {children}
      </div>
    </>
  );
};

Edges.displayName = 'LincleEdges';

export {
  Edges
};

export {
  Path
} from './Paths';
