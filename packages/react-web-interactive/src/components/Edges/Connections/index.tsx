import {
  type ConnectionsProps,
  useConnections
} from '../../../shared';
import {
  Path
} from '../../Edge';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const Connections: FunctionComponent<ConnectionsProps> = ({
  scale = 1,
  translate = {
    x: 0,
    y: 0
  }
}) => {
  const connections = useConnections();

  const paths = useMemo(
    () => {
      return Object.keys(connections).map(
        (
          sourceId
        ) => {
          const {
            line,
            source,
            target
          } = connections[sourceId];

          return (
            <Path
              edgeId={sourceId + 'temp'}
              key={sourceId}
              line={line}
              source={source}
              target={target}
            />
          );
        }
      );
    },
    [
      connections
    ]
  );

  const style = useMemo(
    () => {
      const tx = translate.x;
      const ty = translate.y;
      const tz = scale;

      const transform = `translateX(${tx}px) translateY(${ty}px) scale(${tz})`;

      return {
        transform,
        transformOrigin: 'top left'
      };
    },
    [
      translate.x,
      translate.y,
      scale
    ]
  );

  return (
    <svg
      className='lincle-base-edges'
      style={style}
    >
      {paths}
    </svg>
  );
};

Connections.displayName = 'LincleInteractiveConnections';

export default Connections;
