import {
  Edges as BaseEdges
} from '../../base';
import {
  type EdgesProps,
  useScale,
  useTranslate
} from '../../shared';
import Connections from './Connections';
import {
  type FunctionComponent
} from 'react';

const Edges: FunctionComponent<EdgesProps> = ({
  children,
  ...props
}) => {
  const scale = useScale();
  const translate = useTranslate();

  return (
    <>
      <BaseEdges
        scale={scale}
        translate={translate}
        {...props}
      >
        {children}
      </BaseEdges>
      <Connections
        scale={scale}
        translate={translate}
      />
    </>
  );
};

Edges.displayName = 'LincleInteractiveEdges';

export {
  Edges
};
