import {
  type ModeType,
  useMaxScale,
  useMinScale,
  useModeGiven,
  useOnMode,
  useOnScale,
  useOnTranslate,
  usePan,
  useScale,
  useTranslate,
  useZoom
} from '../../shared';
import Controls from './Controls';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  MapInteraction
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
} from 'react-map-interaction';

const controls = () => {
  return <Controls />;
};

const Interaction: FunctionComponent<object> = () => {
  const scale = useScale();
  const onScale = useOnScale();
  const translate = useTranslate();
  const onTranslate = useOnTranslate();
  const givenMode = useModeGiven();
  const onMode = useOnMode();
  const maxScale = useMaxScale();
  const minScale = useMinScale();
  const pan = usePan();
  const zoom = useZoom();

  const keypress = useRef(false);

  const handleMode = useCallback(
    (changedMode: ModeType) => {
      if (onMode) {
        onMode(changedMode);
      }
    },
    [
      onMode
    ]
  );

  useEffect(
    () => {
      if (givenMode) {
        return (): void => {
          //
        };
      }

      const onKeyDown = (
        event: KeyboardEvent
      ): void => {
        keypress.current = true;

        if (event.shiftKey) {
          handleMode('pull');
        } else if (event.ctrlKey) {
          handleMode('select');
        }
      };

      const onKeyUp = (
        event: KeyboardEvent
      ): void => {
        keypress.current = false;

        if (!event.shiftKey) {
          handleMode('move');
        } else if (!event.ctrlKey) {
          handleMode('move');
        }
      };

      window.addEventListener(
        'keydown',
        onKeyDown
      );

      window.addEventListener(
        'keyup',
        onKeyUp
      );

      return (): void => {
        keypress.current = false;

        window.removeEventListener(
          'keydown',
          onKeyDown
        );

        window.removeEventListener(
          'keyup',
          onKeyUp
        );
      };
    },
    [
      givenMode,
      handleMode
    ]
  );

  const defaultTranslation = useMemo(
    () => {
      return {
        x: Math.round(translate.x),
        y: Math.round(translate.y)
      };
    },
    [] // eslint-disable-line react-hooks/exhaustive-deps
  );

  const handleChange = useCallback(
    (
      {
        scale: z,
        translation: {
          x,
          y
        }
      }: {
        scale: number;
        translation: {
          x: number;
          y: number;
        };
      }
    ) => {
      if (onTranslate) {
        onTranslate({
          x,
          y
        });
      }

      if (onScale) {
        onScale(z);
      }
    },
    [
      onScale,
      onTranslate
    ]
  );

  const transform = useMemo(
    () => {
      return {
        scale,
        translation: translate
      };
    },
    [
      scale,
      translate
    ]
  );

  return (
    <div
      className='lincle-interactive-container'
    >
      <MapInteraction
        defaultScale={scale}
        defaultTranslation={defaultTranslation}
        disablePan={!pan}
        disableZoom={!zoom}
        maxScale={maxScale}
        minScale={minScale}
        onChange={handleChange}
        value={transform}
      >
        {controls}
      </MapInteraction>
    </div>
  );
};

Interaction.displayName = 'LincleInteractiveInteraction';

export default Interaction;
