import {
  type DraggableEvent,
  type PullNodeProps,
  useDiagramId,
  useNodePositions,
  useOnNodeEdgeDrop,
  useScale,
  useSetConnection
} from '../../../shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  DraggableCore,
  type DraggableData as DraggableD,
  type DraggableEvent as DraggableE,
  type DraggableEventHandler
} from 'react-draggable';

const PullNode: FunctionComponent<PullNodeProps> = ({
  className,
  disabled,
  height = 0,
  line = 'direct',
  onDrag,
  onEdgeDrop: givenOnEdgeDrop,
  onStart,
  onStop,
  shape = 'oval',
  sourceId,
  style,
  width = 0,
  x = 0,
  y = 0
}) => {
  const diagramId = useDiagramId();
  const nodePositions = useNodePositions();
  const setConnection = useSetConnection(sourceId as string);
  const onNodeEdgeDrop = useOnNodeEdgeDrop();
  const onEdgeDrop = givenOnEdgeDrop ?? onNodeEdgeDrop;
  const scale = useScale();

  useEffect(
    () => {
      return () => {
        setConnection();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const handleStart = useCallback<DraggableEventHandler>(
    (
      event: DraggableE
    ) => {
      event.stopPropagation();

      if (
        !disabled &&
        onStart
      ) {
        onStart(
          event as DraggableEvent
        );
      }
    },
    [
      disabled,
      onStart
    ]
  );

  const handleDrag = useCallback(
    (
      event: DraggableE,
      data: DraggableD
    ) => {
      event.stopPropagation();

      if (!disabled) {
        setConnection({
          line,
          source: {
            height,
            id: sourceId,
            shape,
            width,
            x,
            y
          },
          target: {
            height: 0,
            id: sourceId,
            shape,
            width: 0,
            x: data.x ?? 0,
            y: data.y ?? 0
          }
        });

        if (onDrag) {
          onDrag(
            event as DraggableEvent,
            data
          );
        }
      }
    },
    [
      disabled,
      height,
      line,
      onDrag,
      setConnection,
      shape,
      sourceId,
      width,
      x,
      y
    ]
  );

  const handleStop = useCallback<DraggableEventHandler>(
    (
      event: DraggableE,
      data: DraggableD
    ) => {
      event.stopPropagation();

      setConnection();

      if (onStop) {
        onStop(
          event as DraggableEvent,
          data
        );
      }

      if (
        onEdgeDrop &&
        nodePositions
      ) {
        nodePositions.match(
          data.x,
          data.y
          // eslint-disable-next-line promise/prefer-await-to-then
        ).then(
          (match) => {
            onEdgeDrop(
              event as DraggableEvent,
              sourceId,
              Boolean(match) || match === 0 ?
                match :
                undefined
            );
          }
          // eslint-disable-next-line promise/prefer-await-to-then
        ).catch(
          () => {
            //
          }
        );
      }
    },
    [
      nodePositions,
      onEdgeDrop,
      onStop,
      setConnection,
      sourceId
    ]
  );

  const viewStyle = useMemo<CSSProperties>(
    () => {
      return {
        borderRadius: shape === 'oval' ?
          50 :
          undefined,
        height,
        left: x,
        position: 'absolute',
        top: y,
        width,
        ...style
      };
    },
    [
      height,
      shape,
      style,
      width,
      x,
      y
    ]
  );

  const nodeRef = useRef(null);

  return diagramId ?

    <DraggableCore
      // defaultPosition={defaultPosition}
      disabled={disabled}
      // isCircle={shape === 'oval'}
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      nodeRef={nodeRef}
      onDrag={handleDrag}
      onStart={handleStart}
      onStop={handleStop}
      // renderSize={height}
      scale={scale}
      // shouldReverse
    >
      <div
        className={className}
        ref={nodeRef}
        style={viewStyle}
      />
    </DraggableCore> :
    null;
};

PullNode.displayName = 'LincleInteractivePullNode';

export default PullNode;
