import {
  type ModeType,
  useModeGiven,
  useOnMode,
  useScale,
  useTranslate
} from '../../../shared';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';

const Interaction: FunctionComponent<PropsWithChildren<object>> = ({
  children
}) => {
  const scale = useScale();
  const translate = useTranslate();
  const givenMode = useModeGiven();
  const onMode = useOnMode();

  const keypress = useRef(false);

  const handleMode = useCallback(
    (changedMode: ModeType) => {
      if (onMode) {
        onMode(changedMode);
      }
    },
    [
      onMode
    ]
  );

  useEffect(
    () => {
      if (givenMode) {
        return (): void => {
          //
        };
      }

      const onKeyDown = (
        event: KeyboardEvent
      ): void => {
        keypress.current = true;

        if (event.shiftKey) {
          handleMode('pull');
        } else if (event.ctrlKey) {
          handleMode('select');
        }
      };

      const onKeyUp = (
        event: KeyboardEvent
      ): void => {
        keypress.current = false;

        if (!event.shiftKey) {
          handleMode('move');
        } else if (!event.ctrlKey) {
          handleMode('move');
        }
      };

      window.addEventListener(
        'keydown',
        onKeyDown
      );

      window.addEventListener(
        'keyup',
        onKeyUp
      );

      return (): void => {
        keypress.current = false;

        window.removeEventListener(
          'keydown',
          onKeyDown
        );

        window.removeEventListener(
          'keyup',
          onKeyUp
        );
      };
    },
    [
      givenMode,
      handleMode
    ]
  );

  const style = useMemo(
    () => {
      const {
        x,
        y
      } = translate;

      return {
        /* stylelint-disable */
        transform: `translateX(${x}px) translateY(${y}px) scale(${scale}) translateZ(0)`
      };
    },
    [
      scale,
      translate
    ]
  );

  return (
    <div
      className='lincle-interactive-graph'
    >
      <div
        className='lincle-interactive-transform'
        style={style}
      >
        {children}
      </div>
    </div>
  );
};

Interaction.displayName = 'LincleInteractiveInteraction';

export default Interaction;
