import Node from '../MiniMapNode';
import {
  type MiniMapPlainProps
} from '../types';
import {
  type NodesDimensions,
  type Size
} from '@lincle/react-shared';
import {
  useNodePositions,
  useScale,
  useTranslate
} from '@lincle/react-web-interactive';
import {
  type FunctionComponent,
  memo,
  useEffect,
  useMemo,
  useState
} from 'react';

const displayName = 'LincleMiniMapPlain';

const MiniMapPlain: FunctionComponent<MiniMapPlainProps> = ({
  className,
  gutter: givenGutter = 8,
  height: givenMapHeight,
  lincleHeight = 0,
  lincleWidth = 0,
  node: nodeFunction,
  style: givenStyle,
  width: givenMapWidth
}) => {
  const nodePositions = useNodePositions();

  const {
    x: translateX,
    y: translateY
  } = useTranslate();

  const scale = useScale();

  const [
    nodes,
    setNodes
  ] = useState<NodesDimensions>();

  const hasNodes = useMemo(
    () => {
      return nodes && Object.keys(nodes).length;
    },
    [
      nodes
    ]
  );

  const [
    nodeSize,
    setNodeSize
  ] = useState<Size>();

  useEffect(
    () => {
      nodePositions?.subscribe(
        displayName,
        (
          newSize,
          newNodes
        ) => {
          setNodeSize(newSize);
          setNodes(newNodes);
        }
      );

      return () => {
        nodePositions?.unsubscribe(displayName);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const map = useMemo(
    () => {
      const piWidth = lincleWidth / Math.PI;
      const piHeight = lincleHeight / Math.PI;

      const width = givenMapWidth ?? Math.min(
        lincleWidth - 32,
        Math.max(
          Math.min(
            piWidth,
            piHeight
          ),
          128
        )
      );
      const height = givenMapHeight ?? Math.min(
        lincleHeight - 32,
        Math.max(
          Math.min(
            piWidth,
            piHeight / 1.5
          ),
          128
        )
      );

      return {
        height,
        width
      };
    },
    [
      givenMapHeight,
      givenMapWidth,
      lincleHeight,
      lincleWidth
    ]
  );

  const viewSize = useMemo(
    () => {
      const height = lincleHeight / scale;
      const left = -translateX / scale;
      const top = -translateY / scale;
      const width = lincleWidth / scale;

      const bottom = top + height;
      const right = left + width;

      return {
        bottom,
        height,
        left,
        right,
        top,
        width
      };
    },
    [
      lincleHeight,
      lincleWidth,
      scale,
      translateX,
      translateY
    ]
  );

  const fullSize = useMemo(
    () => {
      if (
        hasNodes &&
        nodeSize
      ) {
        const bottom = Math.max(
          nodeSize.bottom,
          viewSize.bottom
        );
        const left = Math.min(
          nodeSize.left,
          viewSize.left
        );
        const right = Math.max(
          nodeSize.right,
          viewSize.right
        );
        const top = Math.min(
          nodeSize.top,
          viewSize.top
        );

        const height = bottom - top;
        const width = right - left;

        return {
          bottom,
          height,
          left,
          right,
          top,
          width
        };
      }

      return viewSize;
    },
    [
      hasNodes,
      nodeSize,
      viewSize
    ]
  );

  const viewMaskSize = useMemo(
    () => {
      const scaledHeight = fullSize.height / map.height;
      const scaledWidth = fullSize.width / map.width;

      const viewScale = Math.max(
        scaledWidth,
        scaledHeight
      );

      const viewHeight = viewScale * map.height;
      const viewWidth = viewScale * map.width;
      const gutter = givenGutter * viewScale;

      const left = fullSize.left - (viewWidth - fullSize.width) / 2 - gutter;
      const top = fullSize.top - (viewHeight - fullSize.height) / 2 - gutter;
      const height = viewHeight + gutter * 2;
      const width = viewWidth + gutter * 2;

      const bottom = top + height;
      const right = left + width;

      return {
        bottom,
        gutter,
        height,
        left,
        right,
        top,
        width
      };
    },
    [
      fullSize,
      givenGutter,
      map
    ]
  );

  const Nodes = nodes ?
    Object.keys(nodes).map(
      (nodeId) => {
        const node = nodes[nodeId];

        if (node) {
          return nodeFunction ?
            nodeFunction(node) :
            // eslint-disable-next-line @stylistic/no-extra-parens
            (
              <Node
                height={node.height}
                key={node.id}
                shape={node.shape}
                width={node.width}
                x={node.x}
                y={node.y}
              />
            );
        } else {
          return null;
        }
      }
    ) :
    null;

  // eslint-disable-next-line id-length
  const d = useMemo(
    () => {
      return `M${
        viewMaskSize.left - viewMaskSize.gutter
      },${
        viewMaskSize.top - viewMaskSize.gutter
      }h${
        viewMaskSize.width + viewMaskSize.gutter * 2
      }v${
        viewMaskSize.height + viewMaskSize.gutter * 2
      }h${
        -viewMaskSize.width - viewMaskSize.gutter * 2
      }z M${
        viewSize.left
      },${
        viewSize.top
      }h${
        viewSize.width
      }v${
        viewSize.height
      }h${
        -viewSize.width
      }z`;
    },
    [
      viewMaskSize,
      viewSize
    ]
  );

  const viewBox = useMemo(
    () => {
      return `${
        viewMaskSize.left
      } ${
        viewMaskSize.top
      } ${
        viewMaskSize.width
      } ${
        viewMaskSize.height
      }`;
    },
    [
      viewMaskSize
    ]
  );

  return lincleWidth === 0 ||
    lincleHeight === 0 ?
    null :

    <svg
      className={`${className} lincle-minimap-svg`}
      height={map.height}
      style={givenStyle}
      viewBox={viewBox}
      width={map.width}
    >
      {Nodes}
      <path
        d={d}
        fill='rgba(238, 238, 238, 0.65)'
        fillRule='evenodd'
      />
    </svg>;
};

MiniMapPlain.displayName = displayName;

export default memo(MiniMapPlain);
