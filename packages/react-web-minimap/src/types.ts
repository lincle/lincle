import {
  type Dimensions,
  type Shape
} from '@lincle/react-shared';
import {
  type CSSProperties,
  type ReactElement
} from 'react';

export type MiniMapNodeProps = {
  readonly height: number;
  readonly shape?: Shape;
  readonly width: number;
  readonly x: number;
  readonly y: number;
};

export type MiniMapPlainProps = MiniMapProps & {
  readonly className: string;
  readonly lincleHeight: number;
  readonly lincleWidth: number;
};

export type MiniMapProps = {
  readonly className?: string;
  readonly gutter?: number;
  readonly height?: number;
  readonly node?: (
    dimensions: Dimensions
  ) => ReactElement;
  readonly style?: CSSProperties;
  readonly width?: number;
};
